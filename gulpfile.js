const elixir = require('laravel-elixir');
require('laravel-elixir-vue-2');
elixir.config.assetsPath = 'assets';
elixir.config.publicPath = '';
elixir.config.appPath = '';

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
elixir(mix => {
	mix.less('index.less')
		.less('presentation.less')
		.webpack('index.js')
		.webpack('presentation.js')
		.webpack('worker.js');
});
