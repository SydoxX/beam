window.Vue = require('vue');
Vue.component('app', require('./components/app.vue'));

const vueApp = new Vue({
	el: '#app'
});
