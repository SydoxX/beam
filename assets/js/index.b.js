require('pdfjs-dist');
PDFJS.workerSrc = 'js/worker.js';
var fs = require('fs');
var data = new Uint8Array(fs.readFileSync('present.pdf'));

var electron = require('electron');

var app = electron.remote.app;

var pdfDoc = null;
var pageNum = 1;
var canvasSlides = document.getElementById('canvas-slides');
var contextSlides = canvasSlides.getContext('2d');
var canvasNotes = document.getElementById('canvas-notes');
var contextNotes = canvasNotes.getContext('2d');
contextNotes.translate(-(canvasNotes.width), 0);

const {BrowserWindow} = electron.remote;

var win = null;


function onPresent() {
	if (win != null) {
		return;
	}

	let displays = electron.screen.getAllDisplays();
	let externalDisplay = displays.find((display) => {
		return display.bounds.x !== 0 || display.bounds.y !== 0
	});

	console.log(externalDisplay);

	win = new BrowserWindow({
		x: externalDisplay.bounds.x,
		y: externalDisplay.bounds.y,
		fullscreen: true,
		backgroundColor: '#000000'
	});
	win.loadURL(`file://${app.getAppPath()}/presentation.html`);
	win.on('closed', function () {
		win = null;
	});
}
document.getElementById('present').addEventListener('click', onPresent);


/**
 * Displays previous page.
 */
function onPrevPage() {
	if (pageNum <= 1) {
		return;
	}
	pageNum--;
	pageChanged();
}
document.getElementById('prev').addEventListener('click', onPrevPage);

/**
 * Displays next page.
 */
function onNextPage() {
	if (pageNum >= pdfDoc.numPages) {
		return;
	}
	pageNum++;
	pageChanged();
}
document.getElementById('next').addEventListener('click', onNextPage);

function pageChanged() {
	renderPage(pageNum);
	if (win != null) {
		win.webContents.send('page-changed', pageNum);
	}
}

function renderPage(num) {
	pdfDoc.getPage(num).then(function (page) {

		console.log(page);

		var baseViewport = page.getViewport(1);

		var scale = canvasSlides.height / baseViewport.height;
		var viewport = page.getViewport(scale);

		//
		// Render PDF page into canvas context
		//
		var renderContextSlides = {
			canvasContext: contextSlides,
			viewport: viewport
		};
		page.render(renderContextSlides);

		var renderContextNotes = {
			canvasContext: contextNotes,
			viewport: viewport
		};
		page.render(renderContextNotes);
	});
}

PDFJS.getDocument(data).then(function (pdfDocument) {
	console.log('Number of pages: ' + pdfDocument.numPages);
	pdfDoc = pdfDocument;
	renderPage(pageNum);
});
